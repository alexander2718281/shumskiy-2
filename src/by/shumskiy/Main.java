package by.shumskiy;

public class Main {
    int num1 = (int) (1000 * Math.random());
    int num2 = (int) (1000 * Math.random());
    int num3 = (int) (1000 * Math.random());

    public static void main(String[] args) {
        Main main = new Main();
        System.out.println("Числа: " + main.num1 + " " + main.num2 + " " + main.num3);
        System.out.println("Среднее арифметическое: " + main.averageOfThree(main.num1, main.num2, main.num3));
        main.printLine();
        System.out.println("Результат вычисления: " + main.calculetion(main.num1, main.num2));
    }

    private int averageOfThree(int num1, int num2, int num3){
        int average = (num1 + num2 + num3)/3;
        return average;
    }

    private void printLine(){
        System.out.println("I can do it");
    }

    private int calculetion(int x, int y){
        int result = x;
        result *= y;
        result += x;
        result += y;
        return result;
    }
}
